python-evtx (0.7.4-1) unstable; urgency=medium

  * New upstream version 0.7.4
  * Fix debian/watch
  * Modernize package: Standards-Version, Debhelper compat level

 -- Hilko Bengen <bengen@debian.org>  Fri, 28 Jul 2023 22:23:11 +0200

python-evtx (0.6.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Hilko Bengen ]
  * Fix hexdump import (Closes: #942353)

 -- Hilko Bengen <bengen@debian.org>  Mon, 11 Nov 2019 00:03:40 +0100

python-evtx (0.6.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop Python 2 support (Closes: #937740).
  * Move cli tools from python-evtx to python3-evtx binary package.

 -- Ondřej Nový <onovy@debian.org>  Sun, 27 Oct 2019 20:04:13 +0100

python-evtx (0.6.1-1) unstable; urgency=medium

  * New upstream version 0.6.1
  * Modernize package: Bump Standards-Version, Debhelper compat level

 -- Hilko Bengen <bengen@debian.org>  Mon, 17 Jul 2017 10:09:27 +0200

python-evtx (0.6.0-2) unstable; urgency=medium

  * Fix Python3 dependencies (Closes: #867428)

 -- Hilko Bengen <bengen@debian.org>  Fri, 07 Jul 2017 00:52:04 +0200

python-evtx (0.6.0-1) unstable; urgency=medium

  * New upstream version 0.6.0
  * Scripts are now installed by setup.py, only add them to python-evtx

 -- Hilko Bengen <bengen@debian.org>  Tue, 23 May 2017 09:42:57 +0200

python-evtx (0.5.3b-3) unstable; urgency=medium

  * Add hexdump.py (Closes: #851056)
  * Update watch file

 -- Hilko Bengen <bengen@debian.org>  Thu, 12 Jan 2017 01:30:09 +0100

python-evtx (0.5.3b-2) unstable; urgency=medium

  * Remove patch, fix FTBFS

 -- Hilko Bengen <bengen@debian.org>  Tue, 20 Dec 2016 10:46:43 +0100

python-evtx (0.5.3b-1) unstable; urgency=medium

  * New upstream version 0.5.3b
  * Use Files-Excluded line instead of repacking script
  * Add python*-six dependencies
  * Use PYBUILD_NAME

 -- Hilko Bengen <bengen@debian.org>  Mon, 19 Dec 2016 10:10:09 +0100

python-evtx (0.5.2-1) unstable; urgency=medium

  * New upstream version 0.5.2

 -- Hilko Bengen <bengen@debian.org>  Sun, 18 Dec 2016 15:22:39 +0100

python-evtx (0.5.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Drop version-correcting patch

 -- Hilko Bengen <bengen@debian.org>  Tue, 01 Nov 2016 09:18:12 +0100

python-evtx (0.5.0+dfsg-2) unstable; urgency=medium

  * Fix copy&paste error in description. Thanks, Beatrice Torracca.
    Closes: 837881

 -- Hilko Bengen <bengen@debian.org>  Thu, 15 Sep 2016 08:59:50 +0200

python-evtx (0.5.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Update patch, add "bump version" patch from upstreama
  * Add documentation and script about patched orig tarball
  * Uncomment Vcs-* headers
  * Add Python3 package
  * Bump Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Wed, 14 Sep 2016 00:59:11 +0200

python-evtx (0.3.1+dfsg-1) unstable; urgency=medium

  * New upstream tarball: Removed non-free magazine article
  * Updated watch file

 -- Hilko Bengen <bengen@debian.org>  Mon, 30 Jun 2014 19:21:55 +0200

python-evtx (0.3.1-1) unstable; urgency=low

  * Initial release (Closes: #752181)

 -- Hilko Bengen <bengen@debian.org>  Fri, 20 Jun 2014 17:48:55 +0200
